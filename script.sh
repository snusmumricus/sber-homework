#!/bin/bash
for a in *.tar
do
    a_dir=`expr $a : '\(.*\).tar'`
    mkdir $a_dir 2>/dev/null
    tar -xvf $a -C $a_dir
    cp script.sh $a_dir
    cd $a_dir
    ./script.sh
done

file=*.tar
while [ -e $file ]
do
tar xfv $file
rm $file
file=*.tar
done
